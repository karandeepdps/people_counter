import pandas as pd
import datetime as dt


class CsvWriter:

    def __init__(self,file_name,out_dir):
        self.file_name = file_name
        self.out_dir = out_dir
        self.csv_schema = ['rtsp_link','f_id','person_box','face_box','gender','age','timestamp','file_name']
        self.csv_rows = []

    def create_csv_file(self,data):
        self.df = pd.DataFrame(columns=self.csv_schema,data=data)

    def save_csv_file(self):
        self.create_csv_file(data=self.csv_rows)
        self.df.to_csv(self.out_dir+self.file_name, sep=",", index=False)

    def add_rows(self,row_list):
        self.csv_rows.append(row_list)


if __name__ == "__main__":
    test_csv = CsvWriter(file_name='test.csv', out_dir='./')
    test_csv.add_rows(['rtsp','1','[]','[]','[]','[]',dt.datetime.now(),'[]'])
    test_csv.add_rows(['rtsp','2','[]','[]','[]','[]',dt.datetime.now(),'[]'])
    test_csv.add_rows(['rtsp','3','[]','[]','[]','[]',dt.datetime.now(),'[]'])
    test_csv.add_rows(['rtsp','4','[]','[]','[]','[]',dt.datetime.now(),'[]'])
    test_csv.add_rows(['rtsp','5','[]','[]','[]','[]',dt.datetime.now(),'[]'])


    test_csv.save_csv_file()
