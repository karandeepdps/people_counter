from __future__ import print_function
import requests


addr = 'http://0.0.0.0:5000'
test_url = addr + '/api/test'

content_type = 'image/jpeg'
headers = {'content-type': content_type}



# expected output: {'age': 'Unknown', 'gender': 'Unknown', 'message': 'image received. size=627x627'}
def post_image(img_file):
    img = open(img_file, 'rb').read()
    response = requests.post(test_url, data=img, headers=headers)
    return response.json()

response_pickled = post_image('/Users/Karan/Downloads/Demo/childs.jpg')
print(response_pickled)