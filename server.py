from flask import Flask, request, Response
import jsonpickle
import numpy as np
import cv2
from people_counter import PeopleDetector
from face_detect import GenderPredict, AgePredict, FaceDetector

app = Flask(__name__)

test_obj = PeopleDetector(prototxt='./models/MobileNetSSD_deploy.prototxt',model='./models/MobileNetSSD_deploy.caffemodel')
test_obj._load_model()

genderTest = GenderPredict(prototxt='./models/gender_deploy.prototxt', model='./models/gender_net.caffemodel')
genderTest._load_model()

ageTest = AgePredict(prototxt='./models/age_deploy.prototxt', model='./models/age_net.caffemodel')
ageTest._load_model()

face_detector = FaceDetector(prototxt='./models/opencv_face_detector.pbtxt', model='./models/opencv_face_detector_uint8.pb')
face_detector._load_model()




@app.route('/api/test', methods=['POST'])

def test():
    responses = {}
    counter = 0
    r = request
    # convert string of image data to uint8
    nparr = np.fromstring(r.data, np.uint8)
    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    print(img.shape)

    ####Face Detector

    person_list = test_obj._detect(img)
    if isinstance(person_list, list):
        for person in person_list:
            startX, startY,endX, endY = person
            person_image = img[startY:endY,startX:endX]
            print("person_image shape is ",person_image.shape)
            if  person_image.shape[0] == 0 or person_image.shape[1] == 0:
                continue
            H,W = img.shape[:2]
            face, face_box = face_detector._detect(person_image,H,W)
            if isinstance(face, np.ndarray) and face.shape[0] > 0 and face.shape[1] > 0:
                print("face shape = ", face.shape)
                gender = genderTest._detect(face)
                age = ageTest._detect(face)
                response = {'message': 'image received. size={}x{}'.format(img.shape[1], img.shape[0]),
                            'gender': gender, 'age': age,'person_box':[startX, startY,endX, endY], 'face_box':face_box}
                responses[counter] = (response)

            else:
                print('No face detected')
                response = {'message': 'image received. size={}x{}'.format(img.shape[1], img.shape[0]),
                            'gender': 'Unknown',
                            'age': 'Unknown','person_box':[startX, startY,endX, endY], 'face_box':face_box}
                responses[counter] = (response)
            counter += 1

    response_pickled = jsonpickle.encode(responses)
    return Response(response=response_pickled, status=200, mimetype="application/json")







# start flask app
app.run(host="0.0.0.0", port=5000)