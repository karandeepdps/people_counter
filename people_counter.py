import cv2
import numpy as np


class PeopleDetector:

    def __init__(self, prototxt, model):
        self.CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
                   "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                   "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                   "sofa", "train", "tvmonitor"]

        self.prototxt = prototxt
        self.model = model
        self.confidence = 0.4

    def _load_model(self):
        print("loading model...")
        self.net = cv2.dnn.readNetFromCaffe(self.prototxt, self.model)

    def _detect(self, frame):
        (h, w) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame,(300,300)), 0.007843, (300, 300), 127.5)
        self.net.setInput(blob)
        detections = self.net.forward()
        boxes = []
        confidences = []
        for i in np.arange(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]

            if confidence > self.confidence :

                idx = int(detections[0, 0, i, 1])

                if self.CLASSES[idx] != "person":
                    continue

                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                confidences.append(float(confidence))
                boxes.append([int(startX), int(startY), int(endX), int(endY)])

                cv2.rectangle(frame, (startX, startY), (endX, endY),(255,255,0), 2)

        if (len(boxes)>1):
            indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.5)
            for i in indices:
                i = i[0]
                box = boxes[i]
                left = box[0]
                top = box[1]
                width = box[2]
                height = box[3]

                cv2.rectangle(frame, (left, top), (left+width, top+height), (0,255,0), 2)


        if boxes:
            return (boxes)




if __name__=="__main__":
    test_obj = PeopleDetector(prototxt='./models/MobileNetSSD_deploy.prototxt',model='./models/MobileNetSSD_deploy.caffemodel')
    test_obj._load_model()
    print(test_obj._detect(cv2.imread('/Users/Karan/Downloads/child.jpg')))
