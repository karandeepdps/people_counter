
def get_bboxes(detections, H, W, conf_threshold=0.5):
    bboxes = []
    for i in range(detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > conf_threshold:
            x1 = int(detections[0, 0, i, 3] * W)
            y1 = int(detections[0, 0, i, 4] * H)
            x2 = int(detections[0, 0, i, 5] * W)
            y2 = int(detections[0, 0, i, 6] * H)
            bboxes.append([x1, y1, x2, y2])

    return bboxes


def get_centroid(startX, startY, endX, endY):
    cX = int((startX + endX) / 2.0)
    cY = int((startY + endY) / 2.0)
    return cX,cY



