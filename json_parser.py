class JsonParser:

    def __init__(self):
        pass


    def get_no_of_humans(self, response):
        return len(response)

    def get_person_box(self,response,p_id):
        return response[str(p_id)]['person_box']

    def get_face_box(self,response,p_id):
        return response[str(p_id)]['face_box']

    def get_gender(self,response,p_id):
        return response[str(p_id)]['gender']

    def get_age(self,response,p_id):
        return response[str(p_id)]['age']


if __name__ == "__main__":
    test_response = {'0': {'age': 'Unknown', 'face_box': 0, 'gender': 'Unknown', 'message': 'image received. size=480x360', 'person_box': [173, 208, 247, 346]}, '1': {'age': '(25-32)', 'face_box': [68, 18, 104, 80], 'gender': 'Female', 'message': 'image received. size=480x360', 'person_box': [362, 235, 479, 359]}}
    json_parser = JsonParser()
    nos_person = json_parser.get_no_of_humans(test_response)

    for i in range(0,nos_person):
        print('-'*5,"person_id ",i," ",'-'*5)
        print(json_parser.get_age(test_response,i))
        print(json_parser.get_gender(test_response, i))
        print(json_parser.get_face_box(test_response, i))
        print(json_parser.get_person_box(test_response, i))