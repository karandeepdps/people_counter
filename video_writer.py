import datetime as dt
import cv2
import os


class VideoWriter:

    def __init__(self, output_dir , video_length, frame_rate, frame_shape):
        self.video_length = video_length
        self.last_file_name = ''
        self.output_frame_rate = frame_rate
        self.output_res = frame_shape
        self.writer = None
        self.output_dir = output_dir
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

    def generate_filename(self):
        current_time = dt.datetime.now()
        file_name = current_time.strftime("%Y_%m_%d")
        output_file_name =  "{0}_{1:0=2d}_{2:0=2d}.avi".format(file_name,current_time.hour,self.video_length * int(current_time.minute/self.video_length))
        return output_file_name

    def initialize_video_writer(self):
        self.last_file_name = self.generate_filename()
        self.writer = cv2.VideoWriter(os.path.join(self.output_dir,self.last_file_name), cv2.VideoWriter_fourcc(*'MJPG'),self.output_frame_rate,self.output_res)

    def close_writer(self):
        self.writer.release()

    def get_file_name(self):
        return self.last_file_name

    def write_frame(self, frame):
        self.writer.write(frame)





if __name__ == "__main__":
    test = VideoWriter(output_dir='./output/',video_length=5,frame_rate=15,frame_shape=(640,480))
    test.initialize_video_writer()
    print(test.get_file_name())
