import cv2
from utils import get_bboxes
import numpy as np

class FaceDetector:
    def __init__(self, prototxt, model):
        self.prototxt = prototxt
        self.model = model
        self.confidence = 0.3

    def _load_model(self):
        print("loading model...")
        self.net = cv2.dnn.readNet(self.model, self.prototxt)
        print("loaded model...")

    def _detect(self, frame,H,W):
        frame_copy = frame.copy()
        blob = cv2.dnn.blobFromImage(frame_copy, 1.0, (300, 300), [104, 117, 123], True, False)
        self.net.setInput(blob)
        detections = self.net.forward()
        bboxes = get_bboxes(detections, H, W, conf_threshold=self.confidence)
        print(bboxes)
        if bboxes:
            x1, y1, x2, y2 = bboxes[0]
            return frame_copy[y1:y2,x1:x2],[x1, y1, x2, y2]

        return 0,0


class GenderPredict:

    def __init__(self, prototxt, model):
        self.prototxt = prototxt
        self.model = model
        self.confidence = 0.4
        self.mean_values = (78.4263377603, 87.7689143744, 114.895847746)
        self.CLASSES  = ['Male', 'Female']

    def _load_model(self):
        print("loading model...")
        self.net = cv2.dnn.readNet(self.model, self.prototxt)
        print("loaded model...")

    def _detect(self, face):
        blob = cv2.dnn.blobFromImage(face, 1.0, (227, 227), self.mean_values, swapRB=False)
        self.net.setInput(blob)
        prediction = self.net.forward()
        gender = self.CLASSES[prediction[0].argmax()]
        return gender


class AgePredict:

    def __init__(self, prototxt, model):
        self.prototxt = prototxt
        self.model = model
        self.confidence = 0.5
        self.mean_values = (78.4263377603, 87.7689143744, 114.895847746)
        self.CLASSES = ['(0-2)', '(4-6)', '(8-12)', '(15-20)', '(25-32)', '(38-43)', '(48-53)', '(60-100)']


    def _load_model(self):
        print("loading model...")
        self.net = cv2.dnn.readNet(self.model, self.prototxt)
        print("loaded model...")

    def _detect(self, face):
        blob = cv2.dnn.blobFromImage(face, 1.0, (227, 227), self.mean_values, swapRB=False)
        self.net.setInput(blob)
        prediction = self.net.forward()
        gender = self.CLASSES[prediction[0].argmax()]
        return gender





if __name__=="__main__":
    testObj = FaceDetector(prototxt='./models/opencv_face_detector.pbtxt',model='./models/opencv_face_detector_uint8.pb')
    testObj._load_model()
    cap = cv2.VideoCapture(0)
    while True:
        ret,frame = cap.read()
        f,face_box = testObj._detect(frame)
        if isinstance(f,np.ndarray):
            fx1, fy1, fx2, fy2 = face_box
            cv2.rectangle(frame, (fx1, fy1), (fx2, fy2), (255, 0, 0))
            cv2.imshow('win',frame)
            key = cv2.waitKey(1)
            if key == ord('q'):
                break
        else:
            print('No face detected')
    # genderTest = GenderPredict(prototxt='./models/gender_deploy.prototxt', model='./models/gender_net.caffemodel')
    # genderTest._load_model()
    # gender = genderTest._detect(cv2.imread('./test.jpg'))
    # print(gender)
    # ageTest = AgePredict(prototxt='./models/age_deploy.prototxt', model='./models/age_net.caffemodel')
    # ageTest._load_model()
    # age = ageTest._detect(cv2.imread('./test.jpg'))
    # print(age)
