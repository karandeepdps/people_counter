import cv2
import requests
from video_writer import VideoWriter
import datetime as dt
from csv_writer import CsvWriter
from json_parser import JsonParser

addr = 'http://0.0.0.0:5000'
test_url = addr + '/api/test'
video_length = 1 ## In minutes

rtsp_link = 'http://38.101.209.29:8082/mjpg/video.mjpg'
rtsp_link = 'http://81.149.56.38:8083/mjpg/video.mjpg'
#rtsp_link = 0

content_type = 'image/jpeg'
headers = {'content-type': content_type}
def post_image(img):
    response = requests.post(test_url, data=img, headers=headers)
    return response.json()


if __name__ == "__main__":

    video_stream = cv2.VideoCapture(rtsp_link)
    json_parser = JsonParser()

    if video_stream.isOpened():
        H = video_stream.get(cv2.CAP_PROP_FRAME_HEIGHT)
        W = video_stream.get(cv2.CAP_PROP_FRAME_WIDTH)
        video_writer = VideoWriter(output_dir='./output/', video_length=video_length, frame_rate=video_stream.get(cv2.CAP_PROP_FPS), frame_shape=(int(W), int(H)))
        video_writer.initialize_video_writer()
        file_name = (video_writer.get_file_name())
        last_min = int(dt.datetime.now().minute / video_length)
        csv_writer = CsvWriter(file_name=file_name[:-3]+'csv', out_dir='./output/')
        frame_id = 0

        while True:
            if last_min!=int(dt.datetime.now().minute/video_length):
                frame_id = 0
                video_writer.close_writer()
                csv_writer.save_csv_file()
                last_min = int(dt.datetime.now().minute/video_length)
                video_writer = VideoWriter(output_dir='./output/', video_length=video_length,frame_rate=video_stream.get(cv2.CAP_PROP_FPS), frame_shape=(int(W), int(H)))
                video_writer.initialize_video_writer()
                file_name = (video_writer.get_file_name())
                csv_writer = CsvWriter(file_name=file_name[:-3] + 'csv', out_dir='./output/')
            ret, frame = video_stream.read()

            if ret == False:
                break
            frame_id += 1
            response_pickled = post_image(cv2.imencode('.jpg', frame)[1].tostring())
            print(response_pickled)
            nos_person = json_parser.get_no_of_humans(response_pickled)

            for i in range(0, nos_person):
                age = (json_parser.get_age(response_pickled, i))
                gender = (json_parser.get_gender(response_pickled, i))
                face_box = (json_parser.get_face_box(response_pickled, i))
                person_box = (json_parser.get_person_box(response_pickled, i))
                csv_writer.add_rows([rtsp_link, frame_id, person_box, face_box, gender, age, dt.datetime.now(), file_name])
                x1,y1,x2,y2 = person_box
                if face_box is not 0:
                    fx1,fy1,fx2,fy2 = face_box
                    if not gender == 'Unknown':
                        cv2.putText(frame, gender, (fx1 + 10, fy1 + 10), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0))
                        cv2.rectangle(frame, (fx1, fy1), (fx2, fy2), (255, 0, 0))
                    if not age == 'Unknown':
                     cv2.putText(frame, age, (fx1 + 30, fy1 + 30), cv2.FONT_HERSHEY_COMPLEX, .7, (0, 255, 255))
                cv2.rectangle(frame,(x1,y1),(x2,y2),(255,0,0))
                cv2.putText(frame,'Total No of Persons: {}'.format(nos_person),(20,20),cv2.FONT_HERSHEY_COMPLEX,0.5,(0,255,0))


            key = cv2.waitKey(1)

            if key == ord('q'):
                break

            cv2.imshow('Window', frame)
            video_writer.write_frame(frame)

